# 小学英语语法(yulk 2021-11-09)

## 【一】一般现在时

### 介绍

#### 一、功能

1. 表示事物或人物的特征、状态。如：The sky is blue. 天空是蓝色的。

2. 表示经常性或习惯性的动作。如：I get up at six every day. 我每天六点起床。

3. 表示客观现实。如：The earth goes around the sun. 地球绕着太阳转。

#### 二、构成

1. `be 动词`肯定句：主语+be (am,is,are)+其它。如：I am a boy. 我是一个男孩。
2. `行为动词`肯定句：主语+行为动词(+其它)。  如：We study English. 我们学习英语。
3. `第三人称单数`:  当主语为` he, she,it `时，要在动词后加"`-s`"或"`-es`"。如：Mary likes Chinese. 玛丽喜欢汉语。

> 第三人称单数 `动词+s` 的变化规则: 
>
> 1.一般情况下，直接加`-s`，如：cook-cooks,
>
> 2.以` s. x. sh. ch. o` 结尾 ， 加 `-es` ， 如 ： guess-guesses, wash-washes, watch-watches, go-goes
>
> 3.以“辅音字母+y”结尾，变 y 为 i, 再加-es，如：study-studies特殊：have has

#### 三、变化

1. be 动词的变化。

否定句：主语+ be + not +其它。   如：He is not a worker. 他不是工人。

一般疑问句：Be +主语+其它。       如：-Are you a student?  -Yes. I am. / No, I'm not.

特殊疑问句：疑问词+一般疑问句。如：Where is my bike?

2. 行为动词的变化。

否定句：主语+ don't (doesn't) +动词原形(+其它)。如：I don't like bread.

\* 第三人否定句：当主语为第三人称单数时，要用 doesn't 构成否定句。如：He doesn't often play.

一般疑问句：Do (Does) +主语+动词原形+其它。如：- Do you often play football? - Yes, I do. / No, I don't.

\* 第三人一般疑问句：当主语为第三人称单数时，要用 does 构成一般疑问句。如：- Does she go to work by bike? - Yes, she does. / No, she doesn't.

特殊疑问句：疑问词+一般疑问句 如：How does your father go to work?

### 练习

#### 一、出下列动词的第三人称单数

drink `________` go `________` stay `________` make `________` look `________`

have `________` pass `________` carry `________`  come `________`

watch `________` plant `________` fly `________` study `________`

brush `________` do `________` teach `________` like `________`

play `________` read `________` wash `________` be `________`

#### 二、用括号内动词的适当形式填空。

1. He often `________` (have) dinner at home.

2. Daniel and Tommy `________` (be) in Class One.

3. We `________` (not watch) TV on Monday.

4. Nick `________` (not go) to the zoo on Sunday.

5. `________` they `________` (like) the World Cup?

6. What `________` they often `________` (do) on Saturdays?

7. `________` your parents `________` (read) newspapers every day?

8. The girl `________` (teach) us English on Sundays.

9. She and I `________` (take) a walk together every evening.

10. There `________` (be) some water in the bottle.

11. Mike `________` (like) cooking.

12. They `________` (have) the same hobby.

13. My aunt `________` (look) after her baby carefully.

14. You always `________` (do) your homework well.

15. I `________` (be) ill. I'm staying in bed.

16. She `________` (go) to school from Monday to Friday.

17. Liu Tao `________` (do) not like PE.

18. The child often `________` (watch) TV in the evening.

19. Su Hai and Su Yang `________` (have) eight lessons this term.

20. －What day `________` (be) it today? －It's Saturday.

#### 三、按照要求改写句子

1. Daniel watches TV every evening. (改为否定句)

`_________________________________________________________`

2. I do my homework every day. (改为一般疑问句，作否定回答)

`_________________________________________________________`

3. She likes milk. (改为一般疑问句，作肯定回答)

`_________________________________________________________`

4. Amy likes playing computer games. (改为一般疑问句，作否定回答)

`_________________________________________________________`

5. We go to school every morning. (改为否定句)

`_________________________________________________________`

6. He speaks English very well. (改为否定句)

`_________________________________________________________`

7. I like taking photos in the park. (对划线部分提问)

`_________________________________________________________`

8. John comes from Canada. (对划线部分提问)

`_________________________________________________________`

9. She is always a good student. (改为一般疑问句，作否定回答)

`_________________________________________________________`

10. Simon and Daniel like going skating. (改为否定句)

`_________________________________________________________`

#### 四、改错(划出错误的地方，将正确的写在横线上)

1. Is your brother speak English? `_________________________________________________________`

2. Does he likes going fishing? `_________________________________________________________`
3. He likes play games after class. `_________________________________________________________`
4. Mr. Wu teachs us English. `_________________________________________________________`
5. She don't do her homework on Sundays. `_________________________________________________________`

## 【二】现在进行时

### 介绍

1. 现在进行时表示现在正在进行或发生的动作，也可表示当前一段时间内的活动或现阶段正在进行的动作。

2. 现在进行时的肯定句基本结构为 `be+动词 ing`.

3. 现在进行时的否定句在 `be` 后加 `not`。

4. 现在进行时的一般疑问句把 `be` 动词调到句首。

5. 现在进行时的特殊疑问的基本结构为：`疑问词 + be + 主语 + 动词ing?`

\* 但疑问词当主语时其结构为：`疑问词 + be + 动词ing ?` 标志词：look, now, listen, It's +点钟

> ### 动词加` ing` 的变化规则
>
> 1.一般情况下，直接加 ing，如：cook-cooking
>
> 2.以不发音的 e 结尾，去 e 加 ing
>
> 如： make—making  taste—tasting ,write—writing   ride--riding ,have--having  come—coming  dance—dancing  live—living  take—taking  skat—skating
>
> 3.如果末尾是一个元音字母和一个辅音字母，双写末尾的辅音字母，再加 ing
>
> 如： swim--swimming begin--beginning run-running  sit--sitting put--putting get—getting shop—shopping
>

### 练习

#### 一、写出下列动词的现在分词：

play `________`  run `________`   swim `________`

make `________`    go  `________`     skate `________`

write `________`  ski `________`   read `________` 

have `________` sing `________`  dance `________`

put `________` see `________`  buy `________`

love `________` live `________`  take `________`

come `________` get `________`  stop `________`

sit `________` begin `________`  shop `________`  

#### 二、用所给的动词的正确形式填空：

1. The boy `________` (draw) a picture now.

2. Listen . Some girls `________` (sing) in the classroom .

3. My mother `________` (cook) some nice food now.

4. What `________` you `________` (do) now?

5. Look . They `________` (have) an English lesson .

6. They `________` (not ,water) the flowers now.

7. Look! the girls `________` (dance) in the classroom .

8. What is our granddaughter doing? She `________` (listen) to music.

9. It's 5 o'clock now. We `________` (have) supper now.

10. `________` Helen `________` (wash) clothes? Yes ,she is.

#### 三、句型转换：

1. They are doing housework . (分别改成一般疑问句和否定句)

`_________________________________________________________`

`_________________________________________________________`

2. The students are cleaning the classroom . (改一般疑问句并作肯定和否定回答)

``_________________________________________________________`

``_________________________________________________________`

3. I'm playing the football in the playground . (对划线部分进行提问)

`_________________________________________________________`

4. Tom is reading books in his study . (对划线部分进行提问)

`_________________________________________________________`

## 【三】一般将来时

### 介绍

一、概念：表示将要发生的动作或存在的状态及打算、计划或准备做某事。

句中一般有以下时间状语：tomorrow, next day (week, month, year…),soon, the day after tomorrow（后天）等。

二、基本结构：①be going to + do；②will+ do.

三、否定句：在 be 动词（am, is, are）后加 not 或情态动词 will 后加 not 成 won' t。

例如：I'm going to have a picnic this afternoon. → I'm not going to have a picnic this afternoon.

四、一般疑问句：be 或 will 提到句首，some 改为 any, and 改为 or，第一二人称互换。

例如：We are going to go on an outing this weekend. → Are you going to go on an outing this weekend?

五、一般情况，一般将来时的对划线部分提问有三种情况。

1. 问人。Who

例如：I'm going to New York soon.

→Who's going to New York soon.

2. 问干什么。What … do.

例如：My father is going to watch a race with me this afternoon.

→What is your father going to do with you this afternoon.

3. 问什么时候。When.

例如：She's going to go to bed at nine.

→ When is she going to bed?

六、同义句：be going to = will

I am going to go swimming tomorrow.  =  I will go swimming tomorrow.

### 练习

#### 填空

1. 我打算明天和朋友去野炊。

I `________` have a picnic with my friends.

2. 下个星期一你打算去干嘛? 我想去打篮球。

What `________` next Monday? I `________` play basketball.

What `________` you do next Monday? I `________` play basketball.

3. 你妈妈这个周末去购物吗？是，她要去买一些水果。

`________` your mother `________` go shopping this `________` ?

Yes, she `________` . She `________` buy some fruit.

4. 你们打算什么时候见面。What time `________` you `________` meet?

#### 改句子

5. Nancy is going to go camping. （改否定） Nancy `________` going to go camping.

6. I'll go and join them. （改否定） I `________` go `________` join them.

7. I'm going to get up at 6:30 tomorrow. （改一般疑问句）

 `________` `________` `________` to get up at 6:30 tomorrow?

8. We will meet at the bus stop at 10:30. （改一般疑问句）

 `________` `________` meet at the bus stop at 10:30.

9. She is going to listen to music after school. （对划线部分提问）

 `________` `________` she `________` `________` `________` after school?

10. My father and mother are going to see a play the day after tomorrow. (同上)

 `________` `________` going to see a play the day after tomorrow. 用所给词的适当形式填空。

11. Today is a sunny day. We `________` (have) a picnic this afternoon.

12. My brother `________` (go) to Shanghai next week.

13. Tom often `________` (go) to school on foot. But today is rain. He `________` (go) to school by bike.

14. What do you usually do at weekends? I usually `________` (watch) TV and `________` (catch) insects?

15. It ' s Friday today. What `________` she `________` (do) this weekend?

     She  `________` (watch) TV and `________` (catch) insects. 

16. What `________`(do) you do last Sunday? I `________` (pick) apples on a farm.

What `________` (do) next Sunday? I `________` (milk) cows.

17. Mary `________` (visit) her grandparents tomorrow.

18. Liu Tao `________` (fly) kites in the playground yesterday.

19. David `________` (give) a puppet show next Monday.

20. I `________` (plan) for my study now

## 【四】一般过去时

### 介绍

1.一般过去时表示过去某个时间发生的动作或存在的状态，常和表示过去的时间状语连用。

一般过去时也表示过去经常或反复发生的动作感谢。

2.Be 动词在一般过去时中的变化：

⑴ am 和 is 在一般过去时中变为 was。（was not=wasn't）

⑵ are 在一般过去时中变为 were。（were not=weren't）

⑶ 带有 was 或 were 的句子，其否定、疑问的变化和 is, am, are 一样，即否定句在 was 或 were 后加 not，一般疑问句把 was 或 were 调到句首。

3.句中没有 be 动词的一般过去时的句子

#### 句型变化

否定句：didn't +动词原形，如：Jim didn't go home yesterday.

一般疑问句：在句首加 did，句子中的动词过去式变回原形。如：Did Jim go home yesterday?

特殊疑问句： 

⑴ 疑问词+did+ 主语+ 动词原形？ 如： What did Jim do yesterday?

⑵ 疑问词当主语时：疑问词+动词过去式？如：Who went to home yesterday?

#### 动词变化规则

1. 一般在动词末尾加`-ed`，如：pull-pulled, cook-cooked 2.结尾是 e 加 d，如：taste-tasted

2. 末尾只有`一个元音字母和一个辅音字母的重读闭音节`，应双写末尾的辅音字母，再加-ed，如：stop-stopped

4. 以“`辅音字母+y`”结尾的，变 y 为 i， 再加-ed，如：study-studied

5. 不规则动词过去式：

| 词义 | 现在（原形） | 过去式 |  | 词义 | 现在（原形） | 过去式 |
| ---- | ------------ | ------ | ---- | ------------ | ------ | ------ |
| 是 | am, is (be) | was |  | 忘记 | forget | forgot |
| 是 | are (be) | were |  | 得到 | get | got |
| 成为 | become | became |  | 给 | give | gave |
| 开始 | begin | began |  | 走 | go | went |
| 弯曲 | bend | bent |  | 成长 | grow | grew |
| 吹 | blow | blew |  | 有 | have, has | had |
| 买 | buy | bought |  | 听 | hear | heard |
| 能 | can | could |  | 受伤 | hurt | hurt |
| 捕捉 | catch | caught |  | 保持 | keep | kept |
| 选择 | choose | chose |  | 知道 | know | knew |
| 来 | come | came |  | 学习 | learn | learned, learnt |
| 切 | cut | cut |  | 允许，让 | let | let |
| 做 | do, does | did |  | 躺 | lie | lay |
| 画 | draw | drew |  | 制造 | make | made |
| 饮 | drink | drank |  | 可以 | may | might |
| 吃 | eat | ate |  | 意味 | mean | meant |
| 感觉 | feel | felt |  | 会见 | meet | met |
| 发现 | find | found |  | 必须 | must | must |
| 飞 | fly | flew |  | 放置 | put | put |
| 读 | read | read |  | 将 | shall | should |
| 骑、乘 | ride | rode |  | 唱歌 | sing | sang |
| 响、鸣 | ring | rang |  | 坐下 | sit | sat |
| 跑 | run | ran |  | 睡觉 | sleep | slept |
| 说 | say | said |  | 说 | speak | spoke |
| 看见 | see | saw |  | 度过 | spend | spent |
| 扫 | sweep | swept | | | | |

### 练习

#### 写出下列动词的过去式

is\am `________` fly `________` plant `________`

are `________` drink `________` play `________`

go `________` make `________` does `________`

dance `________` worry `________` ask `________`

taste `________` eat `________` draw `________`

put `________` throw `________` kick `________`

pass `________` do `________`

### Be 动词的过去时练习

#### 用 be 动词的适当形式填空

1. I `________` at school just now. 2. He `________` at the camp last week.

3. We `________` students two years ago. 4. They `________` on the farm a moment ago.

5. Yang Ling `________` eleven years old last year.

6. There `________` an apple on the plate yesterday.

7. There `________` some milk in the fridge on Sunday.

8. The mobile phone `________` on the sofa yesterday evening.

#### 句型转换

1. It was exciting.

否定句：`________________________________`

一般疑问句：`________________________________`

肯、否定回答：`________________________________`

2. All the students were very excited.

否定句：`________________________________`

一般疑问句：`________________________________`

肯定、否定回答：`____________________`  `________________________`

#### 用 be 动词的适当形式填空

1. I `________` an English teacher now.

2. She `________` happy yesterday.

3. They `________` glad to see each other last month.

4. Helen and Nancy `________` good friends.

5. The little dog `________` two years old this year.

6. Look, there `________` lots of grapes here.

7. There `________` a sign on the chair on Monday. .

8. Today `________` the second of June. Yesterday `________` the first of June.

It `________` Children's Day. All the students `________` very excited.

#### 句型转换

1. There was a car in front of the house just now.

否定句：`_________________________________________________________`

一般疑问句：``_________________________________________________________`

肯、否定回答：`_________________________________________________________`

肯、否定回答：`_________________________________________________________`

#### 中译英

1 . 我 的 故 事 书 刚 才 还 在 手 表 旁 边 。

`_________________________________________________________`

2 . 他 们 的 外 套 上 个 星 期 放 在 卧 室 里 了 。

`_________________________________________________________`

3 . 一 会 以 前 花 园 里 有 两 只 小 鸟 。

`_________________________________________________________`

### 行为动词的过去时练习

#### 用行为动词的适当形式填空

1. He `________` (live) in Wuxi two years ago.

2. The cat `________` (eat) a bird last night.

3. We `________` (have) a party last Halloween.

4. Nancy `________` (pick) up oranges on the farm last week.

5. I `________` (make) a model ship with Mike yesterday.

6. They `________` (play) chess in the classroom last PE lesson.

7. My mother `________` (cook) a nice food last Spring Festival.

8. The girls `________` (sing) and _ (dance) at the party.

#### 句型转换

1. Su Hai took some photos at the Sports day.

否定句：`_________________________________________________________`

一般疑问句：`_________________________________________________________`

肯、否定回答：`_________________________________________________________`

2. Nancy went to school early. 

   否 定 句 ：`_________________________________________________________`

一般疑问句：`_________________________________________________________`

肯、否定回答：`_________________________________________________________`

3. We sang some English songs.

否定句：`_________________________________________________________`

一般疑问句：`_________________________________________________________`

肯、否定回答：`_________________________________________________________`

### 综合练习

#### 用 be 动词的适当形式填空

1. I `________` (watch) a cartoon on Saturday.

2. Her father `________` (read) a newspaper last night.

3. We `________` to zoo yesterday, we `________` to the park. (go)

4. `________` you `________` (visit) your relatives last Spring Festival?

5. `________` he `________` (fly) a kite on Sunday? Yes, he _.

6. Gao Shan `________` (pull) up carrots last National Day holiday.

7. I `________`(sweep) the floor yesterday, but my mother `________` .

8. What `________` she `________`(find) in the garden last morning? She `________` (find) a beautiful butterfly.

#### 句型转换

1. They played football in the playground.

否定句：`_________________________________________________________`

一般疑问句：`_________________________________________________________`

肯、否定回答：`_________________________________________________________`

#### 汉译英

1. 格 林 先 生 去 年 住 在 中 国 。

`_________________________________________________________`

2. 昨 天 我 们 参 观 了 农 场 。

`_________________________________________________________`

3. 他 刚 才 在 找 他 的 手 机 。

`_________________________________________________________`

#### 用动词的适当形式填空

1. It `________` (be) Ben's birthday last Friday.

2. We all `________` (have) a good time last night.

3. He `________` (jump) high on last Sports Day.

4. Helen `________` (milk) a cow on Friday.

5. She likes `________` newspapers, but she `________` a book yesterday. (read)

6. He `________` football now, but they `________` basketball just now. (play)

7. Jim's mother `________` (plant) trees just now.

8. `________` they `________` (sweep) the floor on Sunday? No, they `________` .

9. I `________` (watch) a cartoon on Monday. 10. We `________` (go) to school on Sunday.

#### 用动词的适当形式填空

1. It `________`(be) the 2nd of November yesterday.  Mr.White `________` (go) to his office by car.

2. Gao Shan `________` (put) the book on his head a moment ago.

3. Don't `________` the house. Mum `________` it yesterday. (clean)

4. What `________` you `________` just now? I `________` some housework. (do)

5. They `________` (make) a kite a week ago.

6. I want to `________` apples. But my dad `________` all of them last month. (pick)

7. `________` he `________` the flowers this morning? Yes, he `________` . (water)

8. She `________` (be) a pretty girl. Look, she `________` (do) Chinese dances.

9. The students often `________` (draw) some pictures in the art room.

10. What `________` Mike do on the farm? He `________` cows. (milk)

## 【五】Have、Has 和 There be 结构

### 介绍

1、There be 结构包括 there is 、 there are 、 there was 、there were

2、意思都是"有"。

3、和 have、has、had 的区别：

（1） There be 句型表示：在某地有某物（或人）

（2） 在 there be 句型中，主语是单数，be 动词用 is ; 主语是复数，be 动词用 are ; 如有几件物品，be 动词根据最近 be 动词的那个名词决定。即遵循就近原则。

（3） there be 句型的否定句在 be 动词后加 not , 一般疑问句把 be 动词调到句首。

（4） there be 句型与 have (has) 的区别：there be 表示在某地有某物（或人）； have (has) 表示某人拥有某物。

（5） some 和 any 在 there be 句型中的运用：some 用于肯定句， any 用于否定句或疑问句。

（6） and 和 or 在 there be 句型中的运用：and 用于肯定句， or 用于否定句或疑问句。

（7） 针对数量提问的特殊疑问句的基本结构是： How many + 名词复数 + are there + 介词短语？ How much + 不可数名词 + is there + 介词短语？

（8）针对主语提问的特殊疑问句的基本结构是：What's + 介词短语？

（9）There be 结构一般用在句子的开头，而 have 等词只能用于某一个主语后面。

### 练习

#### 使用 `have,has` 或 `there is , there are`填空

1. I `________` a good father and a good mother.

2. `________` a telescope on the desk.

3. He `________` a tape-recorder.

4. `________` a basketball in the playground.

5. She `________` some dresses.

6. They `________` a nice garden.

7. What do you `________` ?

8. `________` a reading-room in the building?

9. What does Mike `________` ?

10. `________` any books in the bookcase?

11. My father `________` a story-book.

12. `________` a story-book on the table.

13. `________` any flowers in the vase?

14. How many students `________` in the classroom?

15. My parents `________` some nice pictures.

16. `________` some maps on the wall.

17. `________` a map of the world on the wall.

18. David `________` a telescope.

19. David's friends `________` some tents.

20. `________` many children on the hill.

#### 用恰当的 `be` 动词填空

1、There `________` a lot of sweets in the box.

2、There `________` some milk in the glass.

3、There `________` some people under the the big tree.

4、There `________` a picture and a map on the wall.

5、There `________` a box of rubbers near the books.

6、There `________` lots of flowers in our garden last year.

7、There `________` a tin of chicken behind the fridge yesterday.

8、There `________` four cups of coffee on the table.

####  使用 `have, has `填空

1. I `________` a nice puppet.

2. He `________` a good friend.

3. They `________` some masks.

4. We `________` some flowers.

5. She `________` a duck.

6. My father `________` a new bike.

7. Her mother `________` a vase.

8. Our teacher `________` an English book.

9. Our teachers `________` a basketball.

10. Their parents `________` some blankets

11. Nancy `________` many skirts.

12. David `________` some jackets.

13. My friends `________` a football.

14. What do you `________` ?

15. What does Mike `________` ?

16. What do your friends `________` ?

17. What does Helen `________` ?

18. His brother `________` a basketball.

19. Her sister `________` a nice doll.

20. Miss Li `________` an English book.

## 【六】名词

### 介绍

#### 可数名词：

表示可以具体个别存在的人或物。可数名词有单复数形式，其单数形式与不定冠词 `a/an` 连用。

可数名词复数规则：

1. 一般情况下，直接加`-s`，如：book-books, bag-bags, cat-cats, bed-beds

2. 以` s. x. sh. ch` 结尾，加`-es`，如：bus-buses, box-boxes, brush-brushes, watch-watches

3 . 以`辅音字母+y`  结尾， 变 `y` 为` i`, 再加`-es` ， 如： family-families, strawberry-strawberries

4. 以`f 或 fe`结尾，变 `f` 或 `fe` 为 `v`, 再加`-es`，如：knife-knives

5. 以结尾 `o` 的通常有生命的加`-es`, 无生命的加`-s` 如：两人三物：黑人 Negro、英雄 hero，马铃薯 potato、西红柿 tomato、芒果 mango。

6. 不规则名词复数：

man-men, woman-women, policeman-policemen, policewoman-policewomen, 

mouse-mice ， child-children, foot-feet, tooth-teeth

fish-fish, people-people, sheep-sheep, Chinese-Chinese, Japanese-Japanese

#### 不可数名词：

表示不能个别存在的事物，如液体类，气体类的物质；少数固体类的物质（grass 草，ice 冰），抽象的名词（help 帮助，music 音乐）。

不可数名词没有复数（如 some water）,不能与不定冠词 a/an 连用。

不可数名词: paper juice water milk rice tea bread hair orange time chicken 不可数名词没有复数形式。

`数量+容器+of+不可数名词`如：a cup of tea 一杯茶

`数量+单位+of+不可数名词`如：a piece of bread 一片面包

### 练习：

一瓶水 `________` 两瓶水 `________` 

一袋大米 `________` 三袋大米 `________` 

一盒牛奶 `________` 四盒牛奶 `________` 

一张纸 `________` 十张纸 `________` 

一公斤鸡肉 `________` 十五公斤鸡肉 `________` 

#### 写出下列各词的复数

photo `________` diary `________` day `________` dress `________` thief `________` yo-yo `________`

peach `________` juice `________` water `________` rice `________` tea `________` man `________`

woman `________` banana `________` bus `________` child `________` foot `________` sheep `________`

leaf(树叶)`________` dish `________` knife `________` pen `________` boy `________` baby `________`

map `________` city `________` box `________` book `________` class `________` eye `________`

office `________` car `________` fox(狐狸) `________` watch `________` library `________` pear `________`

skirt `________` shelf `________` cinema `________` tomato `________` tooth `________` wife `________`

Englishman `________` paper `________` milk `________` Frenchman `________` postman `________` family `________`

mouse `________` people (人们)`________` fish `________` brush `________` mango `________` Japanese `________`

sandwich `________` policeman `________` watermelon `________` Chinese `________` 

strawberry `________` match `________` glass `________`

## 【七】名词所有格

### 介绍

1、名词所有格表示所属关系,相当于物主代词,在句中作定语、宾语或主语。其构成法如下：

(1) 表示人或其它有生命的东西的名词常在词尾加`'s`。

如：Children's Day (儿童节), my sister's book (我姐姐的书)

(2) 以 `s` 或 `es` 结尾的复数名词。只在词尾加' 。如：`Teachers' Day` (教师节)

(3) 有些表示时间、距离以及世界、国家、城镇等无生命的名词,也可在词尾加`'s`.

如：today's newspaper (今天的报纸), ten minutes' break (十分钟的课间休息),China's population (中国的人口).

(4) 无论表示有生命还是无生命的东西的名词,一般均可用介词 of 短语来表示所有关系。

如：a fine daughter of the Party (党的好女儿). 2、[注解]：

①'s 还可以表示某人的家或者某个店铺,如：my aunt's (我阿姨家), the doctor' s (诊所)

② 两人共有某物时, 可以采用 A and B ' s 的形式, 如： Lucy and Lily ' s bedroom (露西和丽丽合住的卧室)

③ “of+名词所有格/名词性物主代词”,称为双重所有格,如：a friend of my father's (我父亲的一位朋友), a friend of mine (我的一位朋友)

### 练习

#### 短语翻译：

1. 我的爷爷              `_________________________________________________________`
2. 杰姆的房间            `_________________________________________________________`
  
3. 我奶奶的钱包          `_________________________________________________________`
  
4. 那些学生的书          `_________________________________________________________`
  
5. 这些工人的外套        `_________________________________________________________`
  
6. 那些小孩的父母        `_________________________________________________________`
  
7. 那些男人的帽子        `_________________________________________________________`
  
8. 那间教室的窗户        `_________________________________________________________`
  
9. 海伦的同学            `_________________________________________________________`
  
10. 这些男孩的床         `_________________________________________________________`
  
11. 汤姆的姑父           `_________________________________________________________`
  
12. 我兄弟的钢笔         `_________________________________________________________`
  
13. 那些老师的书桌       `_________________________________________________________`
  
14. 这些医生的杯子       `_________________________________________________________`
  
15. 那些女孩的座位       `_________________________________________________________`
  
16. 那些女人的自行车     `_________________________________________________________`
  
17. 那些警察的裤子       `_________________________________________________________`
  
18. 那个书包的颜色       `_________________________________________________________`

## 【八】代词

### 介绍：

代词是代替名词以及起名词作用的短语、分句和句子的词。

#### 代词的种类：

人称代词和物主代词

1、人称代词主格和宾格的区别：主格通常位于句中第一个动词之前（有时候位于 than 之后），宾格一般位于动词或介词之后。

2、物主代词形容词性与名词性的区别：形容词性用时后面一般要带上名词，名词性则单独使用，后面不带名词。

| 人称代词 | 主格 | 宾格 |  | 物主代词 | 形容词性 | 名词性 |
| -------------- | ---- | ---- | ---------------- | -------- | ------ | ------ |
| 我 | I | me |  | 我的 | my | mine |
| 你，你们 | you | you |  | 你的，你们的 | your | yours |
| 他 | he | him |  | 他的 | his | his |
| 她 | she | her |  | 她的 | her | hers |
| 它 | it | it |  | 它的 | its | its |
| 我们 | we | us |  | 我们的 | our | ours |
| 他（她，它）们 | they | them |  | 他（她，它）们的 | their | theirs |

1. 人称代词: 主格 I , you, he, she, it, we, you, they 

   ​                宾格 me, you,him, her, it, us, you , them

2. 物主代词: 形容词性的物主代词 my, your, his , her, its, our, your ,their 

   ​                    名词性的物主代词 mine, yours, his,hers, its, ours, yours, theirs

3. 反身代词：myself, yourself, himself, herself, itself, ourselves ,yourselves, themselves. . .

4. 相互代词：each other, one another. . .

5. 指示代词：this , that , these , those ,

6. 疑问代词：who, what, whose. . .

7. 关系代词：which, that, who, whom. . .

8. 连接代词：what, who, whose. . .

9. 不定代词：all, each, both, either, neither, one, any. . .

   没有指明代替任何特定名词或形容词的代词叫做不定代词

#### 使用方法

1. 人称代词是表示“我”、“你”、“他”、“她”、“它”、“我们”、“你们”、“他们”的词。人称代词有人称、数和格的变化。

2. 物主代词表示所有关系的代词，也可叫做代词所有格。物主代词分形容性物主代词和名词性物主代词二种。

3. 表示“我自己”、“你自己”、“他自己”、“我们自己”、“你们自己”和 “他们自己”等的代词，叫做自身代词，也称为“反身代词”。

注意：在连续使用两个以上人称代词时，通常单数 you 放在第一位，I 放在最后；复数 we 放在第一位，they 放在最后。

简单记成：单数 2,3,1，复数 1,2,3。都是三人称，女后男在先。例如：

You and I can help each other. 

They couldn't have seen Tom and me there.

You, Tom and I are leaving next month. 

You or they must pass the exam. 

We, you and they should go there together.

### 练习：

1. That is not `________` kite. That kite is very small, but `________` is very big. (I)
 2. The dress is `________` . Give it to `________` . (she)
3. Is this `________` watch? (you) No, it's not `________` . (I)
4. `________` is my brother. `________` name is Jack. Look! Those stamps are `________` . (he)
5. `________` dresses are red. (we) What colour are `________` ? (you)
6. Here are many dolls, which one is `________` ? (she)
7. I can find my toy, but where's `________` ? (you)
8. Show `________` your kite, OK? (they)
9. I have a beautiful cat. `________` name is Mimi. These cakes are `________` _. (it)
10. Are these `________` tickets? No,`________` are not `________` . `________` aren't here. (they)
11. Shall `________` have a look at that classroom? That is `________` classroom. (we)
12. `________` is my aunt. Do you know `________` job? `________` a nurse. (she)
13. That is not `________` camera. `________` is at home. (he)
14. Where are `________` ? I can't find `________` . Let's call `________` parents. (they)
15. Don't touch `________` . `________` not a cat,`________` a tiger!
16. `________` sister is ill. Please go and get `________` . (she)
17. `________` don't know her name. Would you please tell `________` . (we)
18. So many dogs. Let's count `________` . (they)
19. I have a lovely brother. `________` is only 3. I like `________` very much. (he)
20. May I sit beside `________` ? (you)
21. Look at that desk. Those book are on `________` . (it ） 
22. The girl behind `________` is our friend. (she)

